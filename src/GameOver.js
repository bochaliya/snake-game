import React from 'react';
import axios from 'axios';

export default (props) => {

    /*let topScores = axios.get('http://localhost:3001/topScores/', {})
    console.log(topScores);*/

    let users = [
        { name: 'user1', score: 10 },
        { name: 'user2', score: 9 },
        { name: 'user3', score: 8 },
        { name: 'user4', score: 7 }
    ]
    const listItems = users.map((d) => {
        return <li>User: {d.name} Score: {d.score}</li>
    });
    console.log(listItems);
    return (
        <div>
        <ul>
            {listItems}
        </ul>
        </div>
    )
}