import React from 'react';
import axios from 'axios';
import UserDetails from './UserDetails';
import { render } from 'react-dom';

class GameOver extends React.Component {

    GameOver(props) {
        this.props = props;
    }

    state = {
        topScores: this.getTopScores(),
        topScoresPerUser: this.getTopScoresPerUser(),
        totalGames: this.getTotalGames(),
        totalGamesPerUser: this.getTotalGamesPerUser()
    };

    getTopScores() {
        axios.get('http://localhost:3001/topScores/', {})
            .then(response => {
                console.log(response.data);
                const listItems = response.data.map((d) => {
                    return <li> User: {d.user} Score: {d.score} Score Date: {d.score_date}</li>
                })
                //return listItems;
                this.setState({topScores: listItems});
            })
            .catch((err) => {
                console.log(err);
            })
    }

    getTopScoresPerUser() {
    axios.get('http://localhost:3001/topScores/user/', {})
        .then(response => {
            console.log(response.data);
            const listItems = [];
            response.data.forEach((d) => {
                let user = d._id;
                let topTen = d.topTen;
                topTen.forEach((u)=> {
                    listItems.push(<li> User: {user} Score: {u.score} Score Date: {u.score_date}</li>);
                })
            })
            //return listItems;
            this.setState({topScoresPerUser: listItems});
        })
        .catch((err) => {
            console.log(err);
        })
}

    getTotalGames() {
    axios.get('http://localhost:3001/totalGames/', {})
        .then(response => {
            console.log(response.data);
            const listItems = response.data.map((d) => {
                return <li> Date: {d._id} Total Games: {d.count}</li>
            })
            //return listItems;
            this.setState({totalGames: listItems});
        })
        .catch((err) => {
            console.log(err);
        })
}

    getTotalGamesPerUser() {
    axios.get('http://localhost:3001/totalGames/user/', {})
        .then(response => {
            console.log(response.data);
            const listItems = response.data.map((d) => {
            return <li> User: {d._id.user} Date: {d._id.score_date} Total games: {d.count}</li>
            })
            //return listItems;
            this.setState({totalGamesPerUser: listItems});
        })
        .catch((err) => {
            console.log(err);
        })
}

    render() {
        return (
            <div>
                <h4>Top scores overall</h4>
                <ul>{this.state.topScores}</ul>
                <h4>Top scores per user</h4>
                <ul>{this.state.topScoresPerUser}</ul>
                <h4>Total games per day</h4>
                <ul>{this.state.totalGames}</ul>
                <h4>Total games per day per player</h4>
                <ul>{this.state.totalGamesPerUser}</ul>
            </div>
        );
    }
}

export default GameOver;